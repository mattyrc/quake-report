package com.example.android.quakereport;

public class Earthquake {

    // Global variables
    private double mMagnitude;
    private String mLocation;
    private long mTimeInMilliseconds;
    private String mWebUrl;

    /**
     * Public Constructor Method
     *
     * @param magnitude          is the magnitude (size) of the earthquake
     * @param location           is the location where the earthquake happened
     * @param timeInMilliseconds is the time in milliseconds
     */
    public Earthquake(double magnitude, String location, long timeInMilliseconds, String webUrl) {
        mMagnitude = magnitude;
        mLocation = location;
        mTimeInMilliseconds = timeInMilliseconds;
        mWebUrl = webUrl;
    }

    // Public getter methods
    public double getMagnitude() {
        return mMagnitude;
    }

    public String getLocation() {
        return mLocation;
    }

    public long getTimeInMilliseconds() {
        return mTimeInMilliseconds;
    }

    public String getUrl() {
        return mWebUrl;
    }

}
